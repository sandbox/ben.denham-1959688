<?php

/**
 * @file
 * Default theme implementation for Facebook stream output.
 *
 * Available variables:
 * - $items: An array of Facebook stream posts. With each key as the ID of the
 *           post.
 * -- Each item is an associative array with the following keys:
 * -- id: The unique ID of this Facebook post
 * -- created_time: The creation time of this post formatted as a ISO-8601
 *                  date-time string.
 * -- image: The URL of the profile picture of the user who created this post.
 * -- link: The URL of this post on Facebook.
 * -- message: The message associated with this post.
 */

?>
<table class="fb-stream-wrapper">
   <?php foreach ($items as $item) : ?>
   <tr class="fb-stream-item" id="fb-stream-item-<?php print $item['id']; ?>">
   <td class="fb-stream-img-wrapper">
   <img src="<?php print $item['image']; ?>" />
   </td>
   <td class="fb-stream-item-content-wrapper">
   <div class="fb-stream-item-link-wrapper">
   <a href="<?php print $item['link']; ?>" target="_blank"><?php print $item['message']; ?></a>
   </div>
   <div class="fb-stream-item-date-wrapper">
   <?php print date('M d, Y', strtotime($item['created_time'])); ?>
   </div>
   </td>
   </tr>
   <?php endforeach; ?>
</table>
