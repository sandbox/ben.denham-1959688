<?php

/**
 * @file
 * Admin page callbacks for the fb_stream module.
 */

/**
 * Page callback: FB Status Feed settings.
 */
function fb_stream_form($form, &$form_state) {
  $form['fb_app'] = array(
    '#type' => 'fieldset',
    '#title' => t('Facebook Application Details'),
    '#description' => t("A Facebook application is required to access Facebook's Graph API. If you do not have a Facebook application, you can create one at: !url.", array('!url' => l(t('https://developers.facebook.com/apps'), 'https://developers.facebook.com/apps'))),
    '#collapsible' => TRUE,
  );
  $form['fb_app']['fb_stream_app_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Facebook Application ID'),
    '#default_value' => variable_get('fb_stream_app_id', FB_STREAM_APP_ID),
    '#description' => t('The App ID/API Key of your Facebook application.'),
  );
  $form['fb_app']['fb_stream_app_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Facebook Application Secret'),
    '#default_value' => variable_get('fb_stream_app_secret', FB_STREAM_APP_SECRET),
    '#description' => t('The App secret of your Facebook application.'),
  );
  $form['stream_results_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Stream Results Configuration'),
    '#description' => t('Options for configuring the results displayed in the Facebook stream block.'),
    '#collapsible' => TRUE,
  );
  $form['stream_results_config']['fb_stream_page_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Facebook Page ID'),
    '#default_value' => variable_get('fb_stream_page_id', FB_STREAM_PAGE_ID),
    '#description' => t('The ID of the Facebook page to retrieve the stream from.'),
  );
  $form['stream_results_config']['fb_stream_page_only'] = array(
    '#type' => 'checkbox',
    '#title' => t('Exclude posts by other users.'),
    '#default_value' => variable_get('fb_stream_page_only', FB_STREAM_PAGE_ONLY),
    '#description' => t('If this is option selected, then only posts created by the page itself will be displayed. Posts on the page by other users will be excluded.'),
  );
  $form['stream_results_config']['fb_stream_item_count'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of Items to Display'),
    '#default_value' => variable_get('fb_stream_item_count', FB_STREAM_ITEM_COUNT),
    '#description' => t('The maximum number of items to display in the stream.'),
    '#size' => 5,
  );
  $form['stream_results_config']['fb_stream_allowed_post_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Allowed Facebook Post Types'),
    '#default_value' => variable_get('fb_stream_allowed_post_types', unserialize(FB_STREAM_ALLOWED_POST_TYPES)),
    '#description' => t('The types of Facebook posts to display on the stream. (Leave all checkboxes unticked to allow all post types.)'),
    '#options' => array(
      'status' => t('Status'),
      'link' => t('Link'),
      'photo' => t('Photo'),
      'video' => t('Video'),
      'other' => t('Other'),
    ),
  );
  $form['stream_results_config']['fb_stream_max_characters'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum characters per post link'),
    '#default_value' => variable_get('fb_stream_max_characters', FB_STREAM_MAX_CHARACTERS),
    '#description' => t('Post links exceeding this number of characters will be trimmed. Leave this field as 0 to allow any number of characters.'),
    '#size' => 5,
  );
  $form['advanced_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced Options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['advanced_options']['fb_stream_max_requests'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum Requests'),
    '#default_value' => variable_get('fb_stream_max_requests', FB_STREAM_MAX_REQUESTS),
    '#description' => t("The maximum number of stream requests to make to Facebook's Graph API. Facebook will usually return around 380 items each request, and that will include items that do not meet validation criteria specified on this page. This number does not include the required request for an access token."),
    '#size' => 5,
  );
  $form['advanced_options']['fb_stream_cache_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Cache Length'),
    '#default_value' => variable_get('fb_stream_cache_length', FB_STREAM_CACHE_LENGTH),
    '#description' => t("The number of hours to cache a stream request's results. Set to 0 or leave blank to disable caching."),
    '#size' => 5,
    '#field_suffix' => 'hours',
  );
  $form['fb_platform_policy'] = array(
    '#markup' => t("Note: By using this module to retrieve data via Facebook's Graph API, you agree to !url", array('!url' => l(t("Facebook's Platform Policies"), 'http://developers.facebook.com/policy/'))),
  );
  $form['#submit'][] = 'fb_stream_form_submit';

  return system_settings_form($form);
}

/**
 * Validation handler for fb_stream_form().
 */
function fb_stream_form_validate($form, &$form_state) {
  fb_stream_element_validate_digits_only($form['fb_app']['fb_stream_app_id'], $form_state);
  fb_stream_element_validate_alphanumeric($form['fb_app']['fb_stream_app_secret'], $form_state);
  fb_stream_element_validate_fb_page_id($form['stream_results_config']['fb_stream_page_id'], $form_state);
  element_validate_integer_positive($form['stream_results_config']['fb_stream_item_count'], $form_state);
  fb_stream_element_validate_nonnegative_number($form['stream_results_config']['fb_stream_max_characters'], $form_state);
  element_validate_integer_positive($form['advanced_options']['fb_stream_max_requests'], $form_state);
  fb_stream_element_validate_nonnegative_number($form['advanced_options']['fb_stream_cache_length'], $form_state);
}

/**
 * Submit handler for fb_stream_form().
 */
function fb_stream_form_submit($form, $form_state) {
  // Flush the FB stream cache.
  cache_clear_all('fb_stream', 'cache_block');
}

/**
 * Form element validation handler for digit-only elements.
 */
function fb_stream_element_validate_digits_only($element, &$form_state) {
  $value = $element['#value'];
  if ($value != '' && !preg_match('/^[0-9]*$/i', $value)) {
    form_error($element, t('%name must only contain digits.', array('%name' => $element['#title'])));
  }
}

/**
 * Form element validation handler for alphanumeric elements.
 */
function fb_stream_element_validate_alphanumeric($element, &$form_state) {
  $value = $element['#value'];
  if ($value != '' && !preg_match('/^[a-z0-9]*$/i', $value)) {
    form_error($element, t('%name must only contain alphanumeric characters.', array('%name' => $element['#title'])));
  }
}

/**
 * Form element validation handler for Facebook page IDs.
 */
function fb_stream_element_validate_fb_page_id($element, &$form_state) {
  $value = $element['#value'];
  if ($value != '' && (!preg_match('/^[a-z0-9\.]*$/i', $value))) {
    form_error($element, t('%name must only contain alphanumeric characters and periods (".").', array('%name' => $element['#title'])));
  }
  elseif ($value != '' && strlen($value) < 5) {
    form_error($element, t('%name must be at least five characters.', array('%name' => $element['#title'])));
  }
}

/**
 * Form element validation handler for non-negative number elements.
 */
function fb_stream_element_validate_nonnegative_number($element, &$form_state) {
  $value = $element['#value'];
  if ($value != '' && (!is_numeric($value) || $value < 0)) {
    form_error($element, t('%name must be a non-negative number.', array('%name' => $element['#title'])));
  }
}
