Facebook Stream provides a customisable block that displays a stream of posts
from a Facebook page.

Customisable settings include:

The number of posts to display in the stream. The types of posts to include in
the stream Limiting posts to those created by the Facebook page itself. The
maximum number of requests to make to Facebook in order to retrieve the
specified number of posts that meet your selection criteria. The length of time
to cache the stream data. Because this module makes use of Facebook's Graph API,
you will need to create a Facebook application at:
https://developers.facebook.com/apps

Similar Modules

Facebook Pull (http://drupal.org/project/facebook_pull) also provides a block
to display Facebook posts, but it does not provide options for filtering the
posts that are displayed. Also, while Facebook Pull has a limit for the number
of posts to display, Facebook Stream can be configured to make multiple requests
in order to display the number of posts set on the settings page.

Post-installation configuration:

1. Create a basic Facebook application at: https://developers.facebook.com/apps
2. Use the app ID and app secret from your Facebook application's page to fill
   out the Facebook application details on the Facebook Stream settings page
   (/admin/config/services/fb_stream).
3. Retrieve your Facebook page ID from the end of the URL of the Facebook page
   you want to retrieve the stream from. Facebook Stream will accept numeric
   page IDs, as well as page names from Facebook page vanity URLs. If both a
   numeric ID and page name are contained in the URL, then use the numeric page
   ID.
   Examples:
      URL: http://www.facebook.com/pages/PageName/0000000000
      Page ID to use: 0000000000
      URL: http://www.facebook.com/0000000000
      Page ID to use: 0000000000
      URL: http://www.facebook.com/PageName
      Page ID to use: PageName
3. Enter your Facebook page ID on the Facebook Stream settings page.
4. Adjust any other stream settings on the Facebook Stream settings page.
5. Configure where to display the FB Stream block from the block configuration
   page (/admin/structure/block).
6. If fewer posts are displaying than you
   expect, try increasing the Maximum Requests value under Advanced options on
   the Facebook Stream settings page.
